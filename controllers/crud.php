<?php
/*
 * @author GABRIEL ARIZA GOMES DE CASTRO.
 */
class crud extends database {

    public $sintaxy;

    public function insert($tabela, $campos, $params) {
        /* @author Gabriel Ariza Gomes de Castro


        $tabela = "postagem";
$campos = array(
    'titulo' =>'titulo',
    'descricao' =>'descricao'
);
$params['titulo'] = 'Testando com auto load';
$params['descricao'] = 'Inserido com novo tipo de array';
$crud->insert($tabela, $campos, $params);
echo $crud->lastid;



         * @CamposTabela, Separa o array por virgula criando assim os campos da 
         * tabela no formato correto EX: campo1, campo2, campo3...
         */
        $camposTabela = implode(', ', $campos);
        /////////////////////////////////////////////////////////////////////////
        /* @author Gabriel Ariza Gomes de Castro
         * @Criar as substituições apartir do array de campos.
         * Declara variavel $fields
         * Cria um foreach de campos para recuperar todos os campos.
         * Concatena Fields com " , :"$campos para formar uma sintaxy do tipo...
         * EX , :campo1, :campo2, :campo3, :campo4....
         */
        $fields = '';
        foreach ($campos as $campo) {
            $fields .= ", :" . $campo;
        }
        /* @author Gabriel Ariza Gomes de Castro
         * @retirar ", "(Virgula espaço) da variavel $fields.
         * Conta o tamanho da variavel em strlen
         * Cria uma substring de 2 ate a conta do tamanho da variavel $fields
         * retirando assim da string (Espaço e virgula).
         */
        $contaLeng = strlen($fields);
        $fields = substr($fields, 2, $contaLeng);
        ////////////////////////////////////////////////////////////////////////
        /*@author Gabriel Ariza Gomes de castro.
         * Cria a Sintaxy de uma query PDO de acordo com as extrações do array Campos.
         * Em tabela retorna EX: campo1, campo2, campo3...
         * Em camposTabela retorna EX:  :campo1, :campo2, :campo3, :campo4....
         */
        $this->sintaxy = "INSERT INTO {$tabela} ({$camposTabela}) VALUES ({$fields})";
        ////////////////////////////////////////////////////////////////////////
        /*@author Gabriel Ariza Gomes de castro.
         * Por ultimo passamos então a Sintaxy da query PDO monstada e os Params
         * @$params, Recebe um array do conteúdo que preencherá os campos
         * EX: titulo => Inserindo titulo, Descricao => inserindo descrição
         */
        //echo "<br /><hr>".$this->sintaxy . "<hr ><br />";
        //echo "<br /><hr>".implode(', --- ', $params). "<hr ><br />";
        parent::insertDB($this->sintaxy, $params);
        
        ////////////////////////////////////////////////////////////////////////
    }


    //$sql = "UPDATE postagem SET titulo=:titulo, descricao=:descricao WHERE id=:id";
    public function update($tabela, $campos, $params) {

        $linkscampos = '';
            foreach ($campos as $campo) {
                $linkscampos .= $campo."=:".$campo.", ";
            }
            $contafields = strlen($linkscampos) - 2;
           $linkscampos = substr($linkscampos, 0, $contafields);
          $sql = "UPDATE {$tabela} SET {$linkscampos} WHERE id=:id";

        parent::updateDB($sql, $params);



    }
    
    public function delete($tabela, $params) {

          $sql = "DELETE FROM {$tabela} WHERE id=:id";

        parent::updateDB($sql, $params);



    }
    
    
    public function detect_fields_table($tabele){
        
        $this->sintaxy = "SHOW COLUMNS FROM {$tabele}";
        
      $fields = $this->selectDB($this->sintaxy);
      return $fields;
    }

}
