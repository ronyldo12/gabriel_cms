<?php

/**
 *
 * @author PHP GABRIEL
 */
class pai {

    public $atribut1;
    public $atribut2;
    public $atribut3;

    function __construct($atribut1 = NULL, $atribut2 = NULL, $atribut3 = NULL) {
        $this->atribut1 = "atributo pai";
        $this->atribut2 = $atribut2;
        $this->atribut3 = $atribut3;
    }

    public function load_view($inicio = NULL, $data) {
        $pagina = $data['pagina'];

        if ($inicio != NULL) {

            include "views/" . $inicio . "_view.php";
        } else {
            include "views/" . $pagina . "_view.php";
        }
    }

    public function admin_view($data, $inicio = NULL) {

        if (isset($data['pagina'])) {
            $pagina = $data['pagina'];
        }


        if ($inicio != NULL) {
            include "views/admin/" . $inicio . "_view.php";
        } else {
            include "views/admin/" . $pagina . "_view.php";
        }
    }

    public function uri_segment($int) {

        $conta_url = strlen($_SERVER['QUERY_STRING']);
        $segmento = substr($_SERVER['QUERY_STRING'], 4, $conta_url);
        $segmento = explode('/', $segmento);
       

        return $segmento[$int];

        
    }

    public function current_url() {
        $pageURL = 'http';

        $pageURL .= "://";

        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];

        return $pageURL;
    }

    public function url_maker($string = NULL) {
        $procurar = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í',
            'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á',
            'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô',
            'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', '}', ']', '°', '+', '(', ')', '*', '#', '@', '!', '#', '$', '%', '¨', ':', '’', '‘', ',');
        $substituir = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I',
            'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a',
            'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o',
            'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '', '', '', ' ');
        $replace = str_replace($procurar, $substituir, $string);
        $replace = str_replace(' ', '-', $replace);
        $replace = str_replace(array('-----', '----', '---', '--'), '-', $replace);
        return $replace;
    }

    public function site_name() {

        $site = explode('/', $_SERVER["REQUEST_URI"]);
        $site = $site[1];
        return $site;
    }

    public function base_url($url = NULL) {


        $base_url = "http://" . $_SERVER["SERVER_NAME"] . "/" . $this->site_name() . "/" . $url;
        return $base_url;
    }

    public function base_admin($url = NULL) {


        $base_url = "http://" . $_SERVER["SERVER_NAME"] . "/" . $this->site_name() . "/admin/" . $url;
        return $base_url;
    }

    public function input_post($postagem = NULL) {




        if ($postagem) {
            if(is_array($_POST[$postagem])){
                
            $post = filter_input(INPUT_POST, $postagem, FILTER_DEFAULT , FILTER_REQUIRE_ARRAY);
            
        }else{
            $post = filter_input(INPUT_POST, $postagem);

        }
        } else {
            $post = $_POST;
        }

        return $post;
    }

}
