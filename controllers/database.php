<?php

 class database extends pai{

//     public function __construct() {
//         
//     }
//     public function __clone() {       
//     }
    /* Método construtor do banco de dados */

    /* Método que destroi a conexão com banco de dados e remove da memória todas as variáveis setadas */
    public function __destruct() {
        $this->disconnect();
        foreach ($this as $key => $value) {
            unset($this->$key);
        }
    }

    private static $dbtype = "mysql";
    private static $host = "localhost";
    private static $port = "";
    private static $user = "root";
    private static $password = "";
    private static $db = "gabriel_cms";
    public $lastid;

    /* Metodos que trazem o conteudo da variavel desejada
      @return   $xxx = conteudo da variavel solicitada */

    private function getDBType() {
        return self::$dbtype;
    }
    

    private function getHost() {
        return self::$host;
    }

    private function getPort() {
        return self::$port;
    }

    private function getUser() {
        return self::$user;
    }

    private function getPassword() {
        return self::$password;
    }

    private function getDB() {
        return self::$db;
    }

    private function connect() {
        try {
            $this->conexao = new PDO($this->getDBType() . ":host=" . $this->getHost() . ";port=" . $this->getPort() . ";dbname=" . $this->getDB(), $this->getUser(), $this->getPassword());
        } catch (PDOException $i) {
            //se houver exceção, exibe
            die("Erro: <code>" . $i->getMessage() . "</code>");
        }

        return ($this->conexao);
    }

    private function disconnect() {
        $this->conexao = null;
    }

    /* Método select que retorna um VO ou um array de objetos */
    /*
     * $sql = "SELECT * FROM postagem";
     *
      $postagens = $conexao->selectDB($sql);
      foreach ($postagens as $postagem) {
      echo $postagem->titulo . "<br />";
      echo $postagem->descricao . "<br /><br />";
      }

      //var_dump($conexao->selectDB($sql));
     * 
     */

    public function selectDB($sql, $params = null, $class = null) {
        $query = $this->connect()->prepare($sql);
        $query->execute($params);

        if (isset($class)) {
            $rs = $query->fetchAll(PDO::FETCH_CLASS, $class) or die(print_r($query->errorInfo(), true));
        } else {
            $rs = $query->fetchAll(PDO::FETCH_OBJ);
        }
        self::__destruct();
        return $rs;
    }
   
    /* Método insert que insere valores no banco de dados e retorna o último id inserido */
    /*
     * exemplo de Insert
     */
    /*    $sql="INSERT INTO postagem (titulo, descricao) VALUES (:titulo, :descricao)";
      $params = array(
      'titulo'=>'teste',
      'descricao'=>'testando'
      );
      $lastid = $conexao->insertDB($sql, $params);
      echo $lastid;
     * 
     */
    public function insertDB($sql, $params = null) {
        $conexao = $this->connect();
        $query = $conexao->prepare($sql);
        $query->execute($params);
        $rs = $conexao->lastInsertId() or die(print_r($query->errorInfo(), true));
        self::__destruct();
        $this->lastid = $rs;
        return $this->lastid;
    }
    public function dbNome(){
      $dbNome = self::$db;

      return $dbNome;
    }
    /* Método update que altera valores do banco de dados e retorna o número de linhas afetadas */
    /*
     * @exemplo de update 
     */
    /* $sql = "UPDATE postagem SET titulo=:titulo, descricao=:descricao WHERE id=:id";
      $params = array(
      'titulo'=>'titulo editado',
      'descricao'=>'Descricao teste editado',
      'id'=>10
      );

      $conexao->updateDB($sql, $params);
     * 
     */
    public function updateDB($sql, $params = null) {
        $query = $this->connect()->prepare($sql);
        $query->execute($params);
        $rs = $query->rowCount();
        self::__destruct();
        return $rs;
    }
    /* Método delete que excluí valores do banco de dados retorna o número de linhas afetadas */
    /**
     * @Exemplo  de exclusão Delete
     */
    /*    $sql = "DELETE FROM postagem WHERE id=:id";
      $params = array(
      'id'=>10
      );
      $conexao->deleteDB($sql, $params);
     * 
     */
    public function deleteDB($sql, $params = null) {
        $query = $this->connect()->prepare($sql);
        $query->execute($params);
        $rs = $query->rowCount() or die(print_r($query->errorInfo(), true));
        self::__destruct();
        return $rs;
    }

}
