<?php

/**
 * Description of modulo
 *
 * @author PHP GABRIEL
 */
class modulo extends crud {

    public function index() {


        $data['pagina'] = 'modulo/modulo';

        if ($this->input_post()) {

            $tabela = "cms_modulo";
            $campos = array(
                "modulo" => "modulo",
                "tabela" => "tabela",
                "anexo" => "anexo",
                "extensao" => "extensao",
                "menu" => "menu",
                "tipo" => "tipo"
            );
            $params = array(
                "modulo" => $this->input_post('modulo'),
                "tabela" => $this->input_post('tabela'),
                "anexo" => $this->input_post('anexo'),
                "extensao" => $this->input_post('extensao'),
                "menu" => $this->input_post('menu'),
                "tipo" => $this->input_post('tipo')
            );

            $this->insert($tabela, $campos, $params);

            if ($this->lastid) {
                header("Location:" . $this->base_admin('modulo/campos/' . $this->lastid));
            }
        } else {


            $this->admin_view($data, 'index');
        }
    }

    public function campos() {


        $data['id'] = $this->uri_segment(3);
        $data['pagina'] = 'modulo/campos';
        $this->admin_view($data, 'index');
    }

    public function salvarCampos() {

        $quantidadeCampos = $this->input_post('quantidadeCampos');


        $tabela = "cms_field";
        $campos = array(
            "cms_modulo_id" => "cms_modulo_id",
            "field" => "field",
            "tipo" => "tipo",
            "label" => "label",
            "tamanho" => "tamanho",
            "ckeditor" => "ckeditor",
            "listar" => "listar",
            "table_fk" => "table_fk",
            "fkid" => "fkid",
            "fk_text" => "fk_text"
        );

        for ($i = 1; $i <= $quantidadeCampos; $i++) {
            $this->input_post('field' . $i);

            $valores = array(
                "cms_modulo_id" => $this->uri_segment(3),
                "field" => $this->input_post('field' . $i),
                "tipo" => $this->input_post('tipo' . $i),
                "label" => $this->input_post('descricao' . $i),
                "tamanho" => $this->input_post('tamanho' . $i),
                "ckeditor" => $this->input_post('ckeditor' . $i),
                "listar" => $this->input_post('listar' . $i),
                "table_fk" => $this->input_post('table_fk' . $i),
                "fkid" => $this->input_post('fkid' . $i),
                "fk_text" => $this->input_post('fk_text' . $i)
            );
            $insertField = $this->insert($tabela, $campos, $valores);
        }


        $createTable = '';
        for ($i = 1; $i <= $quantidadeCampos; $i++) {
            $tamanhoChar = $this->input_post('tamanho' . $i);


            if ($this->input_post('tipo' . $i) == "pk") {

                $tipo = 'int';
            } elseif ($this->input_post('tipo' . $i) == "img") {
                $tipo = 'varchar';
                $tamanhoChar = "255";
            } elseif ($this->input_post('tipo' . $i) == "senha") {
                $tipo = 'varchar';
            } else {
                $tipo = $this->input_post('tipo' . $i);
            }

            if ($this->input_post('tipo' . $i) == "pk") {

                $isID = " NOT NULL AUTO_INCREMENT";
            } else {
                $isID = " NOT NULL";
            }


            $createTable .= $this->input_post('field' . $i) . " " . $tipo . " (" . $tamanhoChar . ") " . $isID . ", ";
        }

        $selectTabelaModulo = "SELECT * FROM cms_modulo WHERE id={$this->uri_segment(3)}";
        foreach ($this->selectDB($selectTabelaModulo) as $value) {
            $tabelaCreateFromModulo = $value->tabela;
        }
        $sql2 = "CREATE TABLE IF NOT EXISTS {$tabelaCreateFromModulo} ( " .
                $createTable .
                "PRIMARY KEY (id)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        echo "<a href='".$this->base_admin('modulos')."'>Voltar</a>";
        $this->selectDB($sql2);
    }

    public function menu() {
        if ($this->input_post()) {

            $tabela = "cms_menu";
            $campos = array(
                "menu" => "menu",
                "link" => "link",
                "target" => "target"
            );
            $valores = array(
                "menu" => $this->input_post('menu'),
                "link" => $this->input_post('link'),
                "target" => $this->input_post('target')
            );
            $this->insert($tabela, $campos, $valores);
        } else {
            
        }
        $data['pagina'] = 'modulo/menu';
        $data['title'] = 'menu';

        $this->admin_view($data, 'index');
    }

    public function listar() {


        $data['pagina'] = 'modulo/listar';
        $data['title'] = 'Listar';

        $this->admin_view($data, 'index');
    }

    public function trash() {
        $tabela = $this->input_post('tabela');
        $params = array(
            "id" => $this->input_post('idTrash')
        );
        $this->delete($tabela, $params);
    }

    public function inserir() {
        $id = $this->uri_segment(3);
        $modulo = UnicoregistroPorId('cms_modulo', $id);
        $data['pagina'] = 'modulo/inserir';
        $data['title'] = 'Inserir';


        /*
         * Verifica se tem post ou file
         */
        if ($this->input_post() || $_FILES) {

            /*
             * Se sim verifica se tem post
             */

            if ($this->input_post()) {
                /*
                 * se sim atribui $serializeArray como post
                 * para fazer o foreach como post
                 */

                $serializeArray = $this->input_post();
                $typeArray = 'POST';
            } else {
                $typeArray = 'FILE';
                $serializeArray = $_FILES;
            }
            if ($typeArray == 'POST') {
                foreach ($serializeArray as $key => $value) {
                    $campos[$key] = $key;
                    /*
                     * Verifica se alem de post tem File
                     */
                    if ($_FILES) {
                        /*
                         * se sim continua com o array dos campos da tabela setando também
                         * as imagens
                         */
                        foreach ($_FILES as $chave => $valor) {
                            if ($_FILES[$chave]['name']) {
                                $campos[$chave] = $chave;
                            }
                        }
                    }
                }
            } else {
                /*
                 * Se não for post e for somente File
                 * então seta somente os campos das imagens
                 */
                foreach ($_FILES as $chave => $valor) {
                    if ($_FILES[$chave]['name']) {
                    $campos[$chave] = $chave;
                    }
                }
            }
            /*
             * Verifica novamente se é post
             * 
             */
            if ($typeArray == 'POST') {
                /*
                 * se sim cria o array dos campos dos valores a serem substituidos no PDO bindValue "LINKS PDO"
                 */
                foreach ($this->input_post() as $key => $value) {
                    $params[$key] = $value;
                    if ($_FILES) {
                        /*
                         * Se tem imagem adiciona também no array dos valores a serem substituidos no bind value
                         */
                        foreach ($_FILES as $key => $imagem) {
                            /*
                             * Entra na função de upload aonde moverá a imagem / arquivo para o caminho especificado
                             * e retornará o nome do caminho concatenado com o nome da imagem ou novo
                             * nome da imagem, pois se ja existir uma imagem com o mesmo nome dentro da pasta
                             * a função é encarregada de criar outro nome com md5 time para ser único
                             * por fim termina de inserir o conteudo no array
                             */
                            if ($_FILES[$key]['name']) {
                                $params[$key] = upload_arquivo('public/imagem/gerenciador/', $key);
                            }
                        }
                    }
                }
            } else {
                /*
                 * Se não tem post mas tem File
                 */
                foreach ($_FILES as $key => $imagem) {
                    /*
                     * faz a mesma coisa do exemplo anterior, porem somente com a imagem
                     *
                     */
                    if ($_FILES[$key]['name']) {
                        $params[$key] = upload_arquivo('public/imagem/gerenciador/', $key);
                    }
                }
            }

            /*
             * seta o nome da tabela dinamicamente
             * pegando do field tabela da tabela cms_modulo
             * seleciona o modulo bem aonde o id é ( igual " = " ) o segmento do array da url número 3
             */

            $tabela = $modulo->tabela;



            /*
             * Por fim entra dentro de crud para executar a sintaxy de acordo com os valores do array
             * que criamos, array de campos, array de params que é o array que fará a substituição
             * dos links pdo com os valores quem vem dos input[type="?"] do formulário
             */
            if ($this->input_post('editar')) {

                echo "sexta feira";
                $campos['id'] = 'id';
                $params['id'] = $this->input_post('editar');
                unset($campos['editar']);
                unset($params['editar']);
                $updates = $this->update($tabela, $campos, $params);
                
               // exit;
            } else {
                $this->insert($tabela, $campos, $params);
            }
            /*
             * Te redireciona para a listagem aonde a tabela seja a mesma aonde acaba de inserir / editar
             */
            header("Location:{$this->base_admin('modulo/listar/' . $id)}");
        } else {
            $this->admin_view($data, 'index');
        }
    }

    public function editar() {
        $id = $this->uri_segment(3);
        $modulo = UnicoregistroPorId('cms_modulo', $id);
        $data['pagina'] = 'modulo/editar';
        $data['title'] = 'Editar ' . $modulo->modulo;

        $this->admin_view($data, 'index');
    }
    public function modulos(){
        
        $data['pagina'] = 'modulo/modulos';
        $data['title'] = 'Todos Módulos';
        $this->admin_view($data, 'index');
    }



    public function listarModulos(){
            $data['pagina'] = 'modulo/listarModulos';
        $data['title'] = 'Modulos';
        $this->admin_view($data, 'index');
    }

    public function dropTable(){

        
        
        if($this->input_post('id')){
            $dropTable = "DROP TABLE IF EXISTS {$this->input_post('tabela')}";
            $this->selectDB($dropTable);
            
            $tabela = 'cms_modulo';
            $params = array(
                "id"=>$this->input_post('id')
            );
            $this->delete($tabela, $params);
            
        }

    }

}
