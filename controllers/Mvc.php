<?php

/*
 * @author PHP GABRIEL Ariza Gomes de Castro
 */

class Mvc extends crud {

    function __construct() {

        require 'functions.php';

        /*
         * Verifica a url para ver se tem parametros passados via get $_GET['url'];
         */
        $get = filter_input(INPUT_GET, 'url');
        /*
         * adiciona o / sempre que não existir pois nós temos um array composto de 2 elementos
         */
        $urlGet = rtrim($get, '/');
        /*
         * Separa a string formando um array para fazer listagem dos metodos de um objeto
         */
        $url = explode('/', $urlGet);
        /*
         * Verifica se NÃO foi passado algum parametro pelo url, caso não tenha passado
         *  então define controller inicial como index.php
         */
        if (!$get) {
            $url = "index";
            /*
             * inclui arquivo no código, no caso o arquivo é index.php
             */
            require 'controllers/' . $url . '.php';
            /*
             * instancia i Objeto index
             * $controller = new index;
             */
            $controller = new $url;
            $controller->index();
        } else {

            if ($url[0] == "admin") {

                if (isset($url[1])) {
                    require 'controllers/admin/' . $url[1] . '.php';

                    $controller = new $url[1];
                    if (isset($url[2])) {
                        $controller->{$url[2]}();
                    } else {
                        $controller->index();
                    }
                    
                    
                } else {
                    require 'controllers/admin/' . $url[0] . '.php';

                    $controller = new $url[0];
                    $controller->index();
                }
            } else {
                require 'controllers/' . $url[0] . '.php';
                $controller = new $url[0];
                if (isset($url[1])) {
                    $controller->{$url[1]}();
                } else {
                    $controller->index();
                }
            }
        }
    }

}
