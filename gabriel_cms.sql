-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 20-Ago-2015 às 14:38
-- Versão do servidor: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gabriel_cms`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cms_field`
--

CREATE TABLE IF NOT EXISTS `cms_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cms_modulo_id` int(11) NOT NULL,
  `field` varchar(128) NOT NULL,
  `tipo` varchar(128) NOT NULL,
  `label` varchar(128) NOT NULL,
  `tamanho` varchar(128) NOT NULL,
  `ckeditor` varchar(128) NOT NULL,
  `listar` varchar(128) NOT NULL,
  `table_fk` varchar(128) NOT NULL,
  `fkid` varchar(128) NOT NULL,
  `fk_text` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `cms_field`
--

INSERT INTO `cms_field` (`id`, `cms_modulo_id`, `field`, `tipo`, `label`, `tamanho`, `ckeditor`, `listar`, `table_fk`, `fkid`, `fk_text`) VALUES
(1, 1, 'id', 'pk', 'id', '11', '0', '1', '', '', ''),
(2, 1, 'imagem', 'img', 'imagem', '0', '0', '1', '', '', ''),
(3, 1, 'titulo', 'varchar', 'TÃ­tulo', '255', '0', '1', '', '', ''),
(4, 1, 'descricao', 'text', 'DescriÃ§Ã£o', '0', '0', '1', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cms_menu`
--

CREATE TABLE IF NOT EXISTS `cms_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `target` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `cms_menu`
--

INSERT INTO `cms_menu` (`id`, `menu`, `link`, `target`) VALUES
(1, 'Conteudo', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cms_modulo`
--

CREATE TABLE IF NOT EXISTS `cms_modulo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modulo` varchar(255) NOT NULL,
  `tabela` varchar(255) NOT NULL,
  `anexo` varchar(255) NOT NULL,
  `extensao` varchar(255) NOT NULL,
  `menu` varchar(255) NOT NULL,
  `tipo` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `cms_modulo`
--

INSERT INTO `cms_modulo` (`id`, `modulo`, `tabela`, `anexo`, `extensao`, `menu`, `tipo`) VALUES
(1, 'Gabriel', 'gabriel', '', '', '1', '0');

-- --------------------------------------------------------

--
-- Estrutura da tabela `gabriel`
--

CREATE TABLE IF NOT EXISTS `gabriel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descricao` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
