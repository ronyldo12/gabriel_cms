

<style type="text/css">
    body{
        background: #222;
    }
</style>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html">Myblenet_CMS ADMIN <small>Contato Suporte (62)8331-6640</small></a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>

            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>

            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> USER NAME <b class="caret"></b></a>

            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li class="active">
                    <a href="<?php echo $this->base_admin() ;?>"><i class="fa fa-fw fa-dashboard"></i> Página inicial</a>
                </li>


                <!-- ############################### -->
                <?php

                    
                    $menuPrincipal = "SELECT * FROM cms_menu ORDER BY id ASC";
                   foreach($this->selectDB($menuPrincipal) as $menu){

                ?>
                <li>
                    <a><i class="fa fa-fw fa-file-o"></i> <?php echo $menu->menu; ?></a>
                    
                    <ul>
                    <!-- ##################################### -->
                    <?php
                    $subMenu = "SELECT * FROM cms_modulo WHERE menu={$menu->id}";
                   
                    foreach ($this->selectDB($subMenu) as $sub) {
                       
                    

                   ?>
                        <li><a href="<?php echo $this->base_admin('modulo/listar/'.$sub->id);?>"><?php echo $sub->modulo;?></a></li>
                        <?php
                        }
                        ?>
                       
                    <!-- ##################################### -->

                    </ul>
                </li>
                <?php 
                    }
                ?>
                <!-- ############################### -->

                <li>
                    <a href="<?php echo $this->base_admin('modulo/menu'); ?>"><i class="fa fa-fw fa-edit"></i> Menu</a>
                </li>
                <li>
                    <a> <i class="fa fa-cogs"></i> Modulos</a>

                    <ul>

                        <li><a href="<?php echo $this->base_admin('modulo'); ?>">Criar Modulos</a></li>
                         <li><a href="<?php echo $this->base_admin('modulo/listarModulos');?>">Listar Modulos</a></li>

                    </ul>
                </li> 


            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid">
            <?php
            if (isset($data['conteudo'])) {
                $vari['pagina'] = $data['conteudo'];
                
            } else {
                $vari['pagina'] = 'inicio/meio';
            }


            $this->admin_view($vari);
            ?>
        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>




