<?php
/*
 * @author Gabriel Ariza Gomes de castro.
 * Configuração paginas;
 */

if (isset($data['pagina'])) {
    $vari['pagina'] = $data['pagina'];
} else {

    $vari['pagina'] = 'inicio/meio';
}
?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
    <head>
        <title>Gabriel CMS</title>
<!--        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>-->
         <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="js/gabriel.js"></script>

        <meta charset="UTF-8">
        <meta name="description" content="<?php
        if (isset($data['description'])) {
            echo $data['description'];
        } else {
            echo "description";
        }
        ?>">
        <meta name="keywords" content="<?php
        if (isset($data['keywords'])) {
            echo $data['keywords'];
        } else {
            echo "HTML,CSS,XML,JavaScript";
        }
        ?>">
        <meta name="author" content="Gabriel Ariza Gomes de Castro">


        <meta property="og:image" content="<?php
        if (isset($data['image'])) {
            echo $data['image'];
        } else {
            echo "no-image";
        }
        ?>">
        <meta property="og:image:type" content="<?php
        if (isset($data['imagetype'])) {
            echo $data['imagetype'];
        } else {
            echo "image/jpeg";
        }
        ?>">
        <meta property="og:image:width" content="<?php
        if (isset($data['imagewidth'])) {
            echo $data['imagewidth'];
        } else {
            echo "800";
        }
        ?>">
        <meta property="og:image:height" content="<?php
        if (isset($data['imageheight'])) {
            echo $data['imageheight'];
        } else {
            echo "600";
        }
        ?>"> 



        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?php echo $this->base_url(); ?>public/css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="<?php echo $this->base_url(); ?>public/css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?php echo $this->base_url(); ?>public/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="<?php echo $this->base_url() . "public/css/layout.css"; ?>">
        <script src="<?php echo $this->base_url(); ?>public/js/plugins/morris/raphael.min.js"></script>
        <script src="<?php echo $this->base_url(); ?>public/js/plugins/morris/morris.min.js"></script>
        <script src="<?php echo $this->base_url(); ?>public/js/plugins/morris/morris-data.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


    </head>
    <body>
        <style>
            .row{
                margin:0px;
            }
        </style>
        <?php
        if(isset($data['pagina'])){
            $header['conteudo'] = $data['pagina'];
        }
         $header['pagina'] = 'inicio/header';
        $header['meioadmin'] = 'inicio/meio';
        

        $this->admin_view($header);
        ?>
    </body>
</html>

