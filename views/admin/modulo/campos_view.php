



<div class="content" style="padding-top:20px;padding-bottom:20px;background:#FFF;">
<div class="row">


<?php

//$sql = "SELECT * FROM cms_modulo WHERE id={$this->uri_segment(3)}";
//	var_dump($this->selectDB($sql));

?>



<div class="col-md-12">
<form method="post" action="<?php echo $this->base_admin('modulo/salvarCampos/'.$this->uri_segment(3)) ;?>">
<div class="quantidadeCampos">
<input type="hidden" name="quantidadeCampos" value="0">
</div>
<table class="table table-bordered table-hover table-itens">
<tbody><tr>
            <td>Campo</td>
            <td>Tipo</td>
            <td>Descrição</td>
            <td>Tamanho</td>
            <td>Ckeditor</td>
            <td>Listar</td>
            <td>FK</td>
            <td>FK id</td>
            <td>FK Texto</td>
            <td>x</td>
        </tr>
        </tbody>


</table>



    <div class="col-md-12 text-left">
        <input class="btn btn-primary" style="background:green;" type="submit" value="Salvar">
    </div>
</form>

    <div class="col-md-12 text-right">
        <button class="btn btn-primary" onclick="addItem()" type="button">+ campo</button>
    </div>
</div>

</div>

</div>


 <script>
        var itemId = 1;
        $(function() {
        });

        function atualizaValores() {
            $.each($('.valor_item'), function(index, item) {
                var classHora = $(item).attr('data-classhora');
                var horas = parseFloat($(classHora).val());
                var valorHora = parseFloat($("#valor_hora").val());
                $(item).val(horas * valorHora);
            });
        }

        function atualizaValor(clasHora, elementoHora) {
            var horas = parseFloat($(elementoHora).val());
            var valorHora = parseFloat($("#valor_hora").val());
            $(clasHora).val(horas * valorHora);
        }
        function removeItem(tr) {
        	itemId = itemId -1;
            $('.quantidadeCampos input[type="hidden"]').val(itemId);
            $(tr).remove();

        }

        function getTypeField(itemId, name) {
            var html = '<select class="validate[required]" name="tipo' + itemId + '">';
            
            html += '<optgroup label="Geral">';
            html += '<option value="pk">Chave Primária</div>';
            html += '<option value="img">Imagem/Arquivo</div>';
            html += '<option value="varchar">Campo de texto Simples</div>';
            html += '<option value="text">Texto Área</div>';
            html += '<option value="fk">Relacionamento</div>';
            html += '<option value="senha">Senha</div>';
            html += '<option value="boolean">Boolean(Sim/Não)</div>';
            html += '</optgroup>';
            
            html += '<optgroup label="Data">';
            html += '<option value="date">Data</div>';
            html += '<option value="datetime">Data e Hora</div>';
            html += '<option value="checkbox">Checkbox</div>';
            html += '<option value="hora">Hora</div>';
            html += '</optgroup>';
                      
            html += '<optgroup label="Negócio">';
            html += '<option value="moeda">Monetário</div>';
            html += '<option value="telefone">Telefone</div>';
            html += '<option value="cpf">CPF</div>';
            html += '<option value="cnpj">CNPJ</div>';
            html += '<option value="inteiro">Inteiro</div>';
            html += '<option value="email">E-mail</div>';
            html += '<option value="link_interno">Link Interno Com ID</div>';
            html += '</optgroup>';
            
            html += '</select>';
            return html;
        }

        function getSelectModulos(itemId, $name) {
            var html = '<select style="width:100px;" name="' + $name + itemId + '">';
            html += '<option value="">--</option>';
              
            html += '</select>';
            return html;
        }

        function getOptionTrueOrFalse(itemId, $name, padrao) {
            var html = '<select class="validate[required]" style="width:50px;" name="'+ $name + itemId + '">';
            html += '<option value="">--</div>';
            marcar_true = '';
            marcar_false = '';

            if (padrao == 0)
                marcar_false = 'selected';
            else
                marcar_true = 'selected';

            html += '<option ' + marcar_false + ' value="0">Não</div>';
            html += '<option ' + marcar_true + ' value="1">Sim</div>';
            html += '</select>';
            return html;
        }

        function atualizarCamposFk(idModulo, classSelects) {
           
        }

        function addItem() {
            var html = '<tr id="tr' + itemId + '">';
            html += '<td><input class="validate[required]" style="width:70px;" type="text" name="field'+ itemId+'" /></td>';
            html += '<td>' + getTypeField(itemId) + '</td>';
            html += '<td><input class="validate[required]" type="text" name="descricao' + itemId + '" /></td>';
            html += '<td><input class="validate[required]" style="width:20px;" type="text" name="tamanho' + itemId + '" /></td>';
            //html += '<td>' + getOptionTrueOrFalse(itemId, 'permitir_nulo', 0) + '</td>';
            html += '<td>' + getOptionTrueOrFalse(itemId, 'ckeditor', 0) + '</td>';
            html += '<td>' + getOptionTrueOrFalse(itemId, 'listar', 1) + '</td>';
            html += '<td>' + getSelectModulos(itemId, 'table_fk') + '</td>';
            html += '<td><select style="width:40px;" class="select_id_fk_' + itemId + '" name="fkid' + itemId + '"><option value=""></option></select></td>';
            html += '<td><select style="width:60px;" class="select_id_fk_' + itemId + '" name="fk_text' + itemId + '"><option value=""></option></select></td>';
            $('.quantidadeCampos input[type="hidden"]').val(itemId);
           // html += '<td><input style="width:20px;" type="text" name="item[' + itemId + '][ordem]" value="' + itemId + '" /></td>';
            html += '<td><a onclick="removeItem(\'#tr' + itemId + '\')" href="javascript:void(0)">X</a></td>';
            $('.table-itens tr:last').after(html);
            itemId++;
            jQuery("#validar_formulario").validationEngine();
        }


        	setTimeout(function(){

        	addItem();

        	},300);
    </script>