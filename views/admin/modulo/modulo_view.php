<div class="content" style="background:#FFF;min-height: 550px;border:1px solid #DDD;border-top:none;">
    <div class="row top_page-admin">
        Modulos
    </div>


    <div class="col-md-12" style="margin-top:35px;">
        <form id="formularioModulo" method="post" role="form" class="form-horizontal">

            <div class="buttons" style="text-align: left">


                <div class="form-group">
                    <label class="col-sm-2 control-label">Modulo: </label>
                    <div class="col-sm-10">
                        <input name="modulo" type="text" class="form-control" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Nome tabela: </label>
                    <div class="col-sm-10">
                        <input name="tabela" type="text" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Anexar em: </label>
                    <div class="col-sm-10">
                        <select name="anexo">
                            <option value="">--Nenhuma--</option>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Extensões: </label>
                    <div class="col-sm-10">
                        <input name="extensao" type="text" class="form-control" value="">
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-2 control-label">Menu: </label>
                    <div class="col-sm-10">
                        <select name="menu">
                            <option value="">--Nenhuma--</option>
                            <?php
                            $cmsMenu = "SELECT * FROM cms_menu";
                            $menus = $this->selectDB($cmsMenu);

                            foreach ($menus as $menu) {
                                ?>
                                <option value="<?php echo $menu->id; ?>"><?php echo $menu->menu; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Tipo de registros: </label>
                    <div class="col-sm-10">
                        <select name="tipo">
                            <option value="1">1 registro</option>
                            <option selected="" value="0">registros ilimitados</option>
                            <option value="2">Multi-Upload de imagem</option>
                            <option value="3">Inserir na listagem</option>

                        </select>
                    </div>
                </div>

            </div>
            <div class="buttons" style="text-align: left">
                <input type="submit" class="btn btn-success"  value="Salvar">
                <a class="btn"><i class="icon-remove-circle"></i> Fechar</a>

            </div>
<?php

    

?>
        </form>
    </div>

</div>



<script>
  //  $('#formularioModulo').submit(function(){
        
     //   $.post('<?php echo $this->base_admin('modulo');?>', $('#formularioModulo').serialize(), function(data){
            
    //        alert(data);
     //   });
        
    //    return false;
   // });
    
    
            

</script>