<?php
$idModulo = $this->uri_segment(3);
$tabelaModulo = UnicoregistroPorId('cms_modulo', $idModulo);

//var_dump($tabelaModulo);
?>


<div class="tabela">
    <input type="hidden" name="tabela" value="<?php echo $tabelaModulo->tabela; ?>">
</div>
<div class="content" style="background:#FFF;min-height: 550px;border:1px solid #DDD;border-top:none;height: auto;">
    <div class="row top_page-admin">
        Listagem<?php echo " " . $tabelaModulo->modulo;
?>
    </div>

    <div class="row">
        <div class="col-md-12" style="margin-top:35px;">

            <div class="buttons" style="text-align: left;">
                <a href="<?php echo $this->base_admin('modulo/inserir/' . $idModulo); ?>">
                    <input type="submit" class="btn" style="color:#FFF;background-color: #3071a9;
                           border-color: #285e8e;"  value="Adicionar">
                </a>

                <a class="btn" href="<?php echo $this->base_admin(''); ?>"><i class="icon-remove-circle"></i> Sair</a>

            </div>

        </div>
        <div class="alertbox col-md-12">
            0
        </div>
        <div class="mensagemBox col-md-12 col-sm-12 col-lg-12" style="bottom:-45px;">

        </div>
    </div>
    <div class="row" style="padding:15px;">




        <div class="table-responsive" style="padding-top:30px;margin-top:15px;border-top:1px solid #DDD;">
            <table id="table-list-content" class="table table-bordered table-hover" width="100%" border="0" cellspacing="0" cellpadding="0">

                <thead>
                    <tr class="title">

                        <?php
                        $fieldsTable = $this->detect_fields_table($tabelaModulo->tabela);
                        foreach ($fieldsTable as $fields) {
                            if ($fields->Type != "text") {
                                ?>
                                <td bgcolor="#f3f3f3" valign="top" >
                                    <?php
                                    echo $fields->Field;
                                    if ($fields->Field == "id") {
                                        $tamanhoBase = "width:30px !important;";
                                    } else {
                                        $tamanhoBase = '';
                                    }
                                    ?><br>
                                    <input type="text" style="min-width:10px;max-width:95%;<?php echo $tamanhoBase; ?>" value="" name="<?php echo $fields->Field; ?>" />
                                </td>
                                <?php
                            }
                        }
                        ?>



                        <td bgcolor="#f3f3f3" style="width: 10px">Ver</td>
                        <td bgcolor="#f3f3f3" style="width: 10px">Edi.</td>              
                        <td bgcolor="#f3f3f3" style="width: 10px">Rem</td>
                    </tr>
                </thead>


                <?php
                $sql = "SELECT * FROM {$tabelaModulo->tabela} ORDER BY id DESC";
                foreach ($this->selectDB($sql) as $ModuloTableSelected) {
                    $fields = $this->detect_fields_table($tabelaModulo->tabela);
                    ?>
                    <tbody>
                        <tr style="cursor: pointer" class="trListar <?php echo "trTable" . $ModuloTableSelected->id; ?>">
                            <?php
                            foreach ($fields as $field) {
                                $NameField = $field->Field;
                                if ($field->Type != "text") {
                                    ?>
                            <td style="text-align:center;"><?php
                                        $mystring = $ModuloTableSelected->$NameField;
                                        $findme = 'public/imagem/gerenciador/';
                                        $pos = strpos($mystring, $findme);
                                        if ($pos === false) {
                                            echo $mystring;
                                        } else {
                                            echo '<img src="' . $this->base_url().$mystring . '" height="80px">';
                                        }
                                        // echo $ModuloTableSelected->$NameField;
                                        ?></td>


                                    <?php
                                }
                            }
                            ?>
                            <td style="text-align: center;vertical-align: middle">
                                <a data-toggle="tooltip" target="_blank" style="color:blue; font-size: 18px;"
                                   title="Vizualizar"
                                   href="">
                                    <i class="fa fa-search"></i>
                                </a>
                            </td>
                            <td style="text-align: center;vertical-align: middle">
                                <a href="<?php echo $this->base_admin('modulo/editar/'.$idModulo."/".$ModuloTableSelected->id) ;?>" data-toggle="tooltip" style="color:blue; font-size: 18px;" title="Editar"
                                   href="">
                                    <i class="fa fa-pencil-square-o"></i></a>
                            </td>      
                            <td style="text-align: center;vertical-align: middle">
                                <a data-toggle="tooltip" style="color:red; font-size: 18px;" title="Remover">
                                    <i class="fa fa-trash-o" data-id="<?php echo $ModuloTableSelected->id; ?>" ></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>  
                    <?php
                }
                ?>
            </table>
        </div>
    </div>
</div>




<script>
    $('.fa-trash-o').on('click', function () {
        var ultimoId = $(this).data("id");
        $('.trTable' + ultimoId).css({
            background: "#dff0d8",
            color: "#303030"
        });
        setTimeout(function () {
            $('.trTable' + ultimoId).fadeOut("fast");
        }, 800);

        $(this).hide("slow");
        var CountAlert = $('.alertbox').text();
        CountAlert++;
        $('.alertbox').empty().text(CountAlert);
        $('.mensagemBox').empty().append('<div class="alert alert-success" role="alert"><b>' + CountAlert + ' Registros(s) deletado(s) com sucesso! Ultimo Resgistro N°' + ultimoId);


        $.post('<?php echo $this->base_admin('modulo/trash'); ?>', {
            idTrash: $(this).data("id"),
            tabela: $('.tabela input[type="hidden"]').val()

        }, function (data) {
        });
    });
</script>