<div class="content" style="background:#FFF;min-height: 550px;border:1px solid #DDD;border-top:none;">
    <div class="row top_page-admin">
        Modulos
    </div>


    <div class="col-md-12" style="margin-top:35px;">
        <table class="table table-bordered table-hover" width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr class="title">

                    <td bgcolor="#f3f3f3">Nome Módulo</td>
                    <td bgcolor="#f3f3f3">Nome Tabela</td>
                    <td bgcolor="#f3f3f3">Anexo Tabela</td>
                    <td width="50px" bgcolor="#f3f3f3">Tabela Existente</td>
                    <td width="80px" bgcolor="#f3f3f3">Campos</td>
                    <td width="100px" bgcolor="#f3f3f3">Administrar</td>
                    <td width="100px" bgcolor="#f3f3f3">Excluir</td>
                </tr>
            </thead>
            <tbody>


                <?php
                $modulosSql = "SELECT * FROM cms_modulo";
                $modulos = $this->selectDB($modulosSql);

                foreach ($modulos as $modulo) {
                    ?>
                    <tr class="rows-module trTable<?php echo $modulo->id; ?>">

                        <td>
                            <a title="Editar módulo" href="">
                                <?php echo $modulo->modulo; ?>
                            </a>
                        </td>
                        <td>                   
                            <?php echo $modulo->tabela; ?>
                        </td>
                        <td>
                            <?php echo $modulo->anexo; ?>

                        </td>

                        <td class="text-center">
                            <a title="Remover Tabela" href="">
                                <i style="color:#069" data-id=" <?php echo $modulo->id; ?>" data-tabela=" <?php echo $modulo->tabela; ?>" class="glyphicon glyphicon-ok "></i>
                            </a>
                        </td>
                        <td>   
                            <a href=""><i class="icon-plus-sign"></i> Campos</a>
                        </td>
                        <td>   
                            <a href=""><i class="icon-plus-sign"></i> Administrar</a>
                        </td>
                        <td>    
                            <i style="cursor:pointer;" data-id="<?php echo $modulo->id; ?>" class="fa fa-trash-o"> Remover</i>
                        </td>
                    </tr>
                    <?php
                }
                ?>

            </tbody>
        </table>
    </div>


</div>

<script>
    $('.fa-trash-o').on('click', function () {
         var ultimoId = $(this).data("id");
           $('.trTable' + ultimoId).fadeOut();
            
    });
            $('.glyphicon-ok').on('click', function(){
    var id = $(this).attr("data-id");
            var tabela = $(this).attr("data-tabela");
            $.post('<?php echo $this->base_admin('modulo/dropTable/'); ?>', {
            id      :       id,
                    tabela  :       tabela
            }, function(data){
            alert(data);
            });
    });


</script>