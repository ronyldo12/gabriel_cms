<?php
$idModulo = $this->uri_segment(3);
$tabelaModulo = UnicoregistroPorId('cms_modulo', $idModulo);

//var_dump($tabelaModulo);
?>

<style>
    input[type="file"]{
        opacity: 0 !important;
        top:6px;
        left:0px;
        position:absolute;
    }
    #fileupload{
        height:145px !important;
        top:-110px;
        width:160px;
    }
</style>
<div class="tabela">
    <input type="hidden" name="tabela" value="<?php echo $tabelaModulo->tabela; ?>">
</div>
<div class="content" style="background:#FFF;min-height: 550px;border:1px solid #DDD;border-top:none;height: auto;">
    <div class="row top_page-admin">
        Editar conteudo na tabela (<?php echo $tabelaModulo->tabela; ?>)    
    </div>


    <form name="form1" class="form-horizontal" enctype="multipart/form-data" role="form" id="validar_formulario" method="post" action="<?php echo $this->base_admin('modulo/inserir/' . $idModulo); ?>">
       
                <input type="hidden" name="editar" value="<?php echo $this->uri_segment(4); ?>">
        

        <div class="container-fluid form-modulos">
            <div class="row">
                <div class="panel-body">
                    <?php
                    $fieldsTable = "SELECT * FROM cms_field WHERE cms_modulo_id={$tabelaModulo->id}";

                    $campos = $this->selectDB($fieldsTable);

                    foreach ($campos as $key => $campo) {
                        $tipo = $campo->tipo;
                        $name = $campo->field;
                        $valueField = $campo->field;
                        $valores = UnicoregistroPorId($tabelaModulo->tabela, $this->uri_segment(4));
                        $value = $valores->$valueField;
                       // var_dump($valores->$valueField);

                        //var_dump($value);
                        if ($name != "id") {
                            ?> 
                            <div class="form-group admin-input" style="border-bottom: 1px solid #f4f4f4;padding-bottom: 15px;">
                                <label class="col-sm-2 control-label"><?php echo $campo->label; ?>:</label>
                                <div class="col-sm-10">


                                    <?php
                                    $mystring = $value;
                                    $findme = 'public/imagem/gerenciador/';
                                    $pos = strpos($mystring, $findme);
                                    if ($pos === false) {
                                        
                                    } else {
                                        echo '<div class="imagemEditarModulo col-md-2" style="background:url('.$this->base_url($value).');height:100px;" >'
                                        . ''
                                        . '</div>';
                                    }

                                    echo detect_input($tipo, $name, $value);
                                    unset($value);
                                    ?>
                                </div>
                            </div>    
                            <?php
                        }
                    }
                    ?>
                    <div class="row">
                        <label class="col-sm-2 control-label">

                        </label>
                        <div class="col-xs-10">
                            <hr>
                            <input class="btn btn-primary" type="submit" value="Salvar">

                            <a class="btn" href="<?php echo $this->base_admin('modulo/listar/' . $idModulo); ?>"><i class="icon-remove-circle"></i> Fechar</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>
</div>

<script>

$('#fileupload').on('click', function(){
    
    
    
});


</script>
