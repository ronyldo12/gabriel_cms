<?php
/*
 * @author Gabriel Ariza Gomes de castro.
 * Configuração paginas;
 */

if (isset($data['pagina'])) {
    $pagina = "views/" . $data['pagina'] . "_view.php";
} else {

    $pagina = 'meio_view.php';
}
?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
    <head>
        <title>Gabriel CMS</title>
        <!--        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>-->
         <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="js/gabriel.js"></script>

        <meta charset="UTF-8">
        <meta name="description" content="<?php
        if (isset($data['description'])) {
            echo $data['description'];
        } else {
            echo "description";
        }
        ?>">
        <meta name="keywords" content="<?php
        if (isset($data['keywords'])) {
            echo $data['keywords'];
        } else {
            echo "HTML,CSS,XML,JavaScript";
        }
        ?>">
        <meta name="author" content="Gabriel Ariza Gomes de Castro">


        <meta property="og:image" content="<?php
        if (isset($data['image'])) {
            echo $data['image'];
        } else {
            echo "no-image";
        }
        ?>">
        <meta property="og:image:type" content="<?php
        if (isset($data['imagetype'])) {
            echo $data['imagetype'];
        } else {
            echo "image/jpeg";
        }
        ?>">
        <meta property="og:image:width" content="<?php
        if (isset($data['imagewidth'])) {
            echo $data['imagewidth'];
        } else {
            echo "800";
        }
        ?>">
        <meta property="og:image:height" content="<?php
        if (isset($data['imageheight'])) {
            echo $data['imageheight'];
        } else {
            echo "600";
        }
        ?>"> 

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="<?php echo $this->base_url()."public/css/layout.css" ;?>">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


    </head>
    <body>
        <style>
            .row{
                margin:0px;
            }
        </style>
        <?php
        include 'header_view.php';

        include $pagina;
        
        
        
        include 'footer_view.php';
        ?>
    </body>
</html>

